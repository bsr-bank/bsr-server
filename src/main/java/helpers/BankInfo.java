package helpers;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class BankInfo {

    private static BankInfo ourInstance = new BankInfo();
    public static BankInfo getInstance() {
        return ourInstance;
    }

    private Map<String, String> banks = new HashMap<String, String>();


    private BankInfo() {
    }

    public void addBank(String bankCode, String addr) {
        banks.put(bankCode, addr);
    }

    public String getAddressByBankCode(String code) {
        return banks.get(code);
    }

    public String getBankCodeByAddress(String addr) {
        return banks.entrySet()
                .stream()
                .filter(e -> addr.equals(e.getValue()))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList())
                .get(0);
    }
}
