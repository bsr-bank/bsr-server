package server;

import com.bsr.*;
import helpers.BankInfo;
import helpers.BankUtils;
import models.History;
import models.TransferData;
import models.User;
import mongo.AccountRepository;

import models.Account;
import mongo.HistoryRepository;
import mongo.UserRepository;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import org.springframework.ws.soap.SoapFaultException;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;


@Endpoint
public class SoapBankEndpoint {

    private static final String NAMESPACE_URI = "bsr.com";

    @Value("${auth.username}")
    private String username;
    @Value("${auth.password}")
    private String password;


    @Autowired
    public SoapBankEndpoint() {}

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private HistoryRepository historyRepository;


    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getBalanceRequest")
    @ResponsePayload
    public GetBalanceResponse getBalance(@RequestPayload GetBalanceRequest request) {
        Account account = accountRepository.findAccountByNrb(request.getNrb());

        if (account != null) {
            GetBalanceResponse response = new GetBalanceResponse();
            response.setBalance(account.balance);
            return response;
        } else {
            throw new SoapFaultException("Wybrany numer rachunku nie istnieje.");
        }
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "depositRequest")
    @ResponsePayload
    public DepositResponse deposit(@RequestPayload DepositRequest request) {

        if (request.getAmount() < 1) {
            throw new SoapFaultException("Zła kwota");
        }

        Account account = accountRepository.findAccountByNrb(request.getNrb());

        if (account != null) {
            account.balance += request.getAmount();
            accountRepository.save(account);
            History history = new History("Wpłata", request.getNrb(), request.getAmount());
            historyRepository.save(history);
            DepositResponse response = new DepositResponse();
            response.setBalance(account.balance);
            return response;
        } else {
            throw new SoapFaultException("Wybrany numer rachunku nie istnieje.");
        }
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "withdrawalRequest")
    @ResponsePayload
    public WithdrawalResponse withdrawal(@RequestPayload WithdrawalRequest request) {

        if (request.getAmount() < 1) {
            throw new SoapFaultException("Zła kwota");
        }

        Account account = accountRepository.findAccountByNrb(request.getNrb());

        if (account != null) {
            if (account.balance >= request.getAmount()) {
                account.balance -= request.getAmount();
                accountRepository.save(account);
                History history = new History("Wypłata",request.getNrb(), request.getAmount() * -1);
                historyRepository.save(history);
                WithdrawalResponse response = new WithdrawalResponse();
                response.setBalance(account.balance);
                return response;
            } else {
                throw new SoapFaultException("Brak wystarczających środków.");
            }
        } else {
            throw new SoapFaultException("Wybrany numer rachunku nie istnieje.");
        }
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAccountsRequest")
    @ResponsePayload
    public GetAccountsResponse getAccounts(@RequestPayload GetAccountsRequest request) {

        User user = userRepository.findByUsername(request.getUsername());

        if (user != null) {
            List<Account> accounts = accountRepository.findAllByOwnerId(user.id);
            List<String> nrbs = accounts.stream().map(a -> a.nrb).collect(Collectors.toList());
            GetAccountsResponse response = new GetAccountsResponse();
            response.setNrbs(nrbs);
            return response;
        } else {
            throw new SoapFaultException("Nie znaleziono uzytkownika o podanej nazwie.");
        }
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getHistoryRequest")
    @ResponsePayload
    public GetHistoryResponse getHistory(@RequestPayload GetHistoryRequest request) throws DatatypeConfigurationException {
        List<History> histories = historyRepository.findAllBySourceNrb(request.getNrb());
        for (History h :historyRepository.findAllByDestinationNrb(request.getNrb())) { //kiepskie
            if(!histories.contains(h)) {
                histories.add(h);
            }
        }
        List<com.bsr.History> responseHistories = new ArrayList<>();
        for (History history : histories) {
            com.bsr.History hist = new com.bsr.History();
            hist.setAmount(history.amount);
            hist.setDestinationAccount(history.destinationNrb);
            hist.setSourceAccount(history.sourceNrb);
            hist.setDestinationName(history.destinationName);
            hist.setSourceName(history.sourceName);
            hist.setTitle(history.title);
            GregorianCalendar c = new GregorianCalendar();
            c.setTime(history.date);
            hist.setDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(c));
            responseHistories.add(hist);
        }
        GetHistoryResponse response = new GetHistoryResponse();
        response.setHistories(responseHistories);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "loginRequest")
    @ResponsePayload
    public LoginResponse login(@RequestPayload LoginRequest request) {
        String username = request.getUsername();
        String password = request.getPassword();

        User user = userRepository.findByUsername(username);
        if (user != null) {
            if (password.equals(user.password)) {
                LoginResponse response = new LoginResponse();
                response.setUserId(user.id);
                return response;
            } else {
                throw new SoapFaultException("Błędne hasło.");
            }
        } else {
            throw new SoapFaultException("Użytkownik o takiej nazwie nie istnieje.");
        }
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "transferRequest")
    @ResponsePayload
    public TransferResponse transfer(@RequestPayload TransferRequest request) {

        if (request.getTransfer().getAmount() < 1) {
            throw new SoapFaultException("Zła kwota");
        }

        Transfer transfer = request.getTransfer();
        transfer.setDestinationAccount(transfer.getDestinationAccount().replaceAll("\\s+", ""));
        Account account = accountRepository.findAccountByNrb(transfer.getSourceAccount());
        if (account != null) {
            if (account.balance >= transfer.getAmount()) {
                RestTemplate restTemplate = new RestTemplate();
                String uri = "http://" + BankInfo.getInstance().getAddressByBankCode(BankUtils.getBankCodeFrom(transfer.getDestinationAccount())) + "/accounts/" + transfer.getDestinationAccount() + "/history/";
                TransferData data = new TransferData();

                data.setSource_account(transfer.getSourceAccount());
                data.setAmount(transfer.getAmount());
                data.setTitle(transfer.getTitle());
                data.setSource_name(transfer.getSourceName());
                data.setDestination_name(transfer.getDestinationName());

                HttpEntity<Object> entity = new HttpEntity<Object>(data, getHeaders());
                ResponseEntity<TransferData> restResponseData = restTemplate.postForEntity(uri, entity, TransferData.class);
                if (restResponseData.getStatusCode().equals(HttpStatus.CREATED)) {
                    account.balance -= transfer.getAmount();
                    accountRepository.save(account);
                    History history = new History(
                            transfer.getSourceAccount(),
                            transfer.getDestinationAccount(),
                            transfer.getAmount(),
                            transfer.getTitle(),
                            transfer.getSourceName(),
                            transfer.getDestinationName()
                    );
                    historyRepository.save(history);
                    TransferResponse response = new TransferResponse();
                    response.setBalance(account.balance);
                    return response;
                } else {
                    throw new SoapFaultException("Błąd wewnętrzny.");
                }
            } else {
                throw new SoapFaultException("Brak wystarczających środków.");
            }
        } else {
            throw new SoapFaultException("Błąd.");
        }
    }

    private HttpHeaders getHeaders() {
        String plainCredentials = username + ":" + password;   //"test:test";
        String base64Credentials = new String(Base64.encodeBase64(plainCredentials.getBytes()));

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Credentials);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        return headers;
    }
}
