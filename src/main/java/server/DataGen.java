package server;

import helpers.BankInfo;
import helpers.NrbService;
import models.Account;
import models.User;
import mongo.AccountRepository;
import mongo.UserRepository;
import nl.garvelink.iban.IBAN;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.iban4j.Bic;
import org.iban4j.CountryCode;
import org.iban4j.Iban;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.io.*;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Random;

@Repository
public class DataGen {

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private UserRepository userRepository;

    @PostConstruct
    public void init() {
        downloadAndReadCsv();
    }



    private void downloadAndReadCsv() {
        try {
            //InputStream input = new URL("http://example.com/file.csv").openStream();
            InputStream input = new FileInputStream(new File("/Users/krzysztof/Desktop/banks.csv"));
            Reader reader = new InputStreamReader(input);

            CSVFormat format = CSVFormat.newFormat(' ');
            CSVParser parser = new CSVParser(reader, format);

            List<CSVRecord> records = parser.getRecords();
            if (records != null) {
                for (CSVRecord record : records) {
                    BankInfo.getInstance().addBank(record.get(0), record.get(1));
                }
            }

            parser.close();
            reader.close();
            input.close();

//            populateDatabase();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void populateDatabase() {
        User user0 = new User("user", "user");
        User user1 = new User("John Doe", "1234");
        User user2 = new User("John Smith", "1234");
        userRepository.save(user0);
        userRepository.save(user1);
        userRepository.save(user2);
        Account account0 = new Account(user0.id, createNewNrb(), 0);
        Account account1 = new Account(user1.id, createNewNrb(), 1000);
        Account account2 = new Account(user1.id, createNewNrb(), 2018);
        Account account3 = new Account(user2.id, createNewNrb(), 0);

        accountRepository.save(account0);
        accountRepository.save(account1);
        accountRepository.save(account2);
        accountRepository.save(account3);
    }

    private String createNewNrb() {
        Random random = new Random();
        InetAddress ip = null;

        try {
            ip = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        String accountNo = "";
        for (int i = 0; i < 16 ; i++) {
            accountNo += random.nextInt(9);
        }

        NrbService nrb = new NrbService();
//        Iban iban = new Iban.Builder()
//                .countryCode(CountryCode.PL)
//                .bankCode("00116911")
//                .buildRandom();

//        String bankcode = (ip != null) ?  BankInfo.getInstance().getBankCodeByAddress(ip.getHostAddress()) : "12345678";
//        String nrb = "00" + bankcode;
//        for (int i = 0; i < 16 ; i++) {
//            nrb += random.nextInt(9);
//        }

//        String nrb = iban.toString();


        return nrb.generateNrb();
    }
}
