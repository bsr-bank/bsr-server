package server;

import mongo.AccountRepository;
import mongo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories(basePackageClasses = {AccountRepository.class, UserRepository.class, DataGen.class})
public class Application {

//    @Autowired
//    private DataGen gen;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }


}
