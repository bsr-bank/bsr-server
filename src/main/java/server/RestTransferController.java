package server;

import com.fasterxml.jackson.databind.ObjectMapper;
import helpers.NrbService;
import models.Account;
import models.History;
import models.TransferData;
import mongo.AccountRepository;
import mongo.HistoryRepository;
import mongo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("accounts/{nrb}/history")
public class RestTransferController {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private HistoryRepository historyRepository;

    private static final ObjectMapper mapper = new ObjectMapper();



    @RequestMapping(method = RequestMethod.POST)
    ResponseEntity<?> transfer(@PathVariable String nrb, @RequestBody TransferData transferData) {
        ResponseEntity response;

        NrbService nrbService = new NrbService();
        if (!nrbService.validateNrb(nrb) || !nrbService.validateNrb(transferData.getSource_account())) {
            response = ResponseEntity.badRequest().build();
            return response;
        }
        if (transferData.getTitle().isEmpty() || transferData.getSource_account().isEmpty()) {
            response = ResponseEntity.badRequest().build();
            return response;
        }

        Account account = accountRepository.findAccountByNrb(nrb);

        if (account != null) {
            if (transferData.getAmount() > 0) {
                History history = new History(
                        transferData.getSource_account(),
                        nrb,
                        transferData.getAmount(),
                        transferData.getTitle(),
                        transferData.getSource_name(),
                        transferData.getDestination_name()
                );
                historyRepository.save(history);
                account.balance += transferData.getAmount();
                accountRepository.save(account);
                response = ResponseEntity.created(URI.create("accounts/" + nrb)).build();
            } else {
                response = ResponseEntity.badRequest().build();
            }
        } else {
            response = ResponseEntity.notFound().build();
        }

        return response;
    }
}
