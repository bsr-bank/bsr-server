package models;

import org.springframework.data.annotation.Id;

import java.util.Date;

public class History  {
    @Id
    public String id;

    public Date date;
    public String sourceNrb;
    public String destinationNrb;
    public int amount;
    public String title;
    public String sourceName;
    public String destinationName;

    public History() {

    }

    public History(String id, Date date, String sourceNrb, String destinationNrb, int amount, String title, String sourceName, String destinationName) {
        this.id = id;
        this.date = date;
        this.sourceNrb = sourceNrb;
        this.destinationNrb = destinationNrb;
        this.amount = amount;
        this.title = title;
        this.sourceName = sourceName;
        this.destinationName = destinationName;
    }

    public History(String sourceNrb, String destinationNrb, int amount, String title, String sourceName, String destinationName) {
        this.sourceNrb = sourceNrb;
        this.destinationNrb = destinationNrb;
        this.amount = amount;
        this.title = title;
        this.sourceName = sourceName;
        this.destinationName = destinationName;
        this.date = new Date();
    }

    public History(String title, String nrb, int amount) {
        this.title = title;
        this.sourceNrb = nrb;
        this.destinationNrb = nrb;
        this.amount = amount;
        this.date = new Date();
    }

    @Override
    public boolean equals(Object obj) {
        History h = (History) obj;
        return h.id.equals(id);
    }
}
