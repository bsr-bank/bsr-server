package models;

public class TransferData {

    private String source_account;
    private int amount;
    private String title;
    private String source_name;
    private String destination_name;

    public TransferData(String source_account, int amount, String title, String source_name, String destination_name) {
        this.source_account = source_account;
        this.amount = amount;
        this.title = title;
        this.source_name = source_name;
        this.destination_name = destination_name;
    }

    public TransferData() {

    }

    public String getSource_account() {
        return source_account;
    }

    public int getAmount() {
        return amount;
    }

    public String getTitle() {
        return title;
    }

    public String getSource_name() {
        return source_name;
    }

    public String getDestination_name() {
        return destination_name;
    }

    public void setSource_account(String source_account) {
        this.source_account = source_account;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSource_name(String source_name) {
        this.source_name = source_name;
    }

    public void setDestination_name(String destination_name) {
        this.destination_name = destination_name;
    }
}
