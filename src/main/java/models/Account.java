package models;

import org.springframework.data.annotation.Id;

public class Account {
    @Id
    public String id;

    public String ownerId;
    public String nrb;
    public int balance;

    public Account(String ownerId, String nrb, int balance) {
        this.ownerId = ownerId;
        this.nrb = nrb;
        this.balance = balance;
    }

    @Override
    public String toString() {
        return String.format("Account[id=%s, nrb='%s', balance='%s']", id, nrb, balance);
    }


}
