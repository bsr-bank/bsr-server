package mongo;


import models.History;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface HistoryRepository extends MongoRepository<History, String> {
//    public List<History> findAllBySourceNrbOrDestinationNrb(String sourceNrb, String destinationNrb);
    public List<History> findAllBySourceNrb(String nrb);
    public List<History> findAllByDestinationNrb(String nrb);

}
