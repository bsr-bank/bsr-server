package mongo;

import models.Account;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface AccountRepository extends MongoRepository<Account, String>{
    public Account findAccountByNrb(String nrb);
    public List<Account> findAllByOwnerId(String id);
}
